package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Happy case: all parameters are matching")
    public void newEmployeeCreatedAllParametersMatching(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Act
        Employee employeeMaria = new Employee(firstName, lastName, description, jobTitle, email);

        //Assert
        assertEquals(firstName, employeeMaria.getFirstName());
        assertEquals(lastName, employeeMaria.getLastName());
        assertEquals(description, employeeMaria.getDescription());
        assertEquals(jobTitle, employeeMaria.getJobTitle());
        assertEquals(email, employeeMaria.getEmail());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: FistName can be Empty")
    public void newEmployeeNotCreatedFirstNameEmpty(){

        //Arrange
        String firstName = "";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee must have First Name", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: FistName can be Null")
    public void newEmployeeNotCreatedFirstNameNull(){

        //Arrange
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(null, lastName, description, jobTitle, email));
        assertEquals("Employee must have First Name", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: Last Name can be Empty")
    public void newEmployeeNotCreatedLastNameEmpty(){

        //Arrange
        String firstName = "Maria";
        String lastName = "";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee must have Last Name", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: FistName can be Null")
    public void newEmployeeNotCreatedLastNameNull(){

        //Arrange
        String firstName = "Maria";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, null, description, jobTitle, email));
        assertEquals("Employee must have Last Name", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: Description can be Empty")
    public void newEmployeeNotCreatedDescriptionEmpty(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee must have Description", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: Description can be Null")
    public void newEmployeeNotCreatedDescriptionNull(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, null, jobTitle, email));
        assertEquals("Employee must have Description", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: JobTitle can be Empty")
    public void newEmployeeNotCreatedJobTitleEmpty(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee must have JobTitle", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: JobTitle can be Null")
    public void newEmployeeNotCreatedJobTitleNull(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String email = "maria@gmail.com";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, null, email));
        assertEquals("Employee must have JobTitle", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: E-Mail can be Empty")
    public void newEmployeeNotCreatedEMailEmpty(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee must have E-Mail", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: E-Mail can be Null")
    public void newEmployeeNotCreatedEMailNull(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, null));
        assertEquals("Employee must have E-Mail", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: E-Mail don't have @")
    public void newEmployeeNotCreatedEMailDoNotHaveAtSymbol(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "mariagmail.com";

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("E-mail must have '@'", thrown.getMessage());
    }

}